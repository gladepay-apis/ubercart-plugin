# GladePay Ubercart Payment Plugin

Payment plugin for Ubercart '8.x-3.0' core: '8.x' which implements ubercart offsite payment method and integrates with Gladepay Inline-Checkout

Support email: a.anthony@gladepay.com

Ubercart is an ecommerce extension which runs on Drupal Content Management System

# Prerequisite
1. Shop owner has signed up with GladePay as a Merchant at: https://dashboard.gladepay.com/register
2. Shop owner has Drupal 8 installed
3. Shop owner has Ubercart module installed on Drupal 8

Synopsis
---------
This module serve as Payment Gateway porvided by Gladepay.

Main Module: uc_gladepay.module
Requried Modules: commerce, pmphone(provided in module itself)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
------------
1. Download Drupal 8 from https://www.drupal.org/8
2. Install Drupal 8 (see steps @ https://www.drupal.org/docs/8/install) 
3. Download and setup ubercart module
4. Configure your store and setup products and their prices (Ensure to set NGN as store currency)
3. Install the module (See Installation steps on how to)
4. Goto Store>>Payment Methods.
5. Select Gladepay and click on “settings” against it. 
6. You can either choose TEST or LIVE based on whether you are using Live Merchant ID or Key or the demo Merchant ID and Key.

# Installation steps
1. Download the plugin from https://developer.gladepay.com/sdk-plugins/ and decompress the plugin folder
2. Place the plugin folder within the 'modules' folder of your drupal site directory
3. Go to Admin Dashboard of your Drupal 8 installation
4. Click on Extend Tab
![](images/1.jpg)
5. Search for 'Gladepay', select the module, and click install
![](images/2.jpg)
6. Go to 'Store' tab and click 'Payment Methods'
![](images/3.jpg)
7. Select 'Gladepay Inline Checkout' as the type of payment method and click 'Add Payment method'
![](images/4.jpg)
8. Fill in the configuration form for TEST mode (NB: Use the Merchant Key and Merchant ID from your GladePay merchant dashboard as your 'live' credentials)  and Save
![](images/5.jpg)
![](images/6.jpg)
9. Go to Store and shop normally.

# Paying with Gladepay at checkout
1. Customer enters billing information during checkout and Selects 'Gladepay' as payment method during checkout
![](images/7.jpg)
3. Gladepay Inline Checkout modal pops up and customer makes payment
![](images/8.jpg)
4. Store owner can view orders
![](images/9.jpg)
5. Store owner can view transaction status
![](images/10.jpg)
![](images/11.jpg)
6. Customer can view and print invoice (NB: Billing and shipping address information is gotten from the ubercart store and not from gladepay. Hence invoices would contain those information for a fully setup store. This store is only for test purposes)
![](images/12.jpg)
![](images/13.jpg)

## Authors
GladePay.
Contributor and Support (email): a.anthony@gladepay.com

## Acknowledgments
*Drupal and Ubercart Developer Support Forums
*Existing ubercart payment plugin developers and payment gateway companies