<?php

namespace Drupal\uc_gladepay\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\uc_order\Entity\Order;
use Drupal\uc_order\OrderInterface;
use Drupal\uc_gladepay\Plugin\Ubercart\PaymentMethod\GladepayRedirect;

/**
 * Returns responses for Gladepay routes.
 */


class GladepayController extends ControllerBase {

  /**
   * Handles a complete Gladepay Payments Standard sale.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to the cart or checkout complete page.
   */
  
  public function GladepayComplete() {
    //RESPONSES FROM GATEWAY GOTTEN AS GET VARIABLES.

      //get full response from gladepay
        $gladepayTxnRef = json_decode($_GET['response'], true);
        $order_id = json_decode($_GET['order_id']); //get txnid from cart sent with GET
    
        //GET transaction reference from gladepay for verification purposes
        $txnRef = $gladepayTxnRef['txnRef'];

        //get order details from checkout for invoice
        \Drupal::entityManager()->getStorage('uc_order')->resetCache([$order_id]);
        $order = Order::load($order_id);

      //get charged amount
      $amount = $gladepayTxnRef['chargedAmount'];

    switch ($gladepayTxnRef['txnStatus']) {
        case 'successful':
         $comment = $this->t('Gladepay mihpayid : @txn_id', ['@txn_id' => $txnRef]);
         uc_payment_enter($order_id, 'uc_gladepay', $amount, $order->getOwnerId(), NULL, $comment);
         uc_order_comment_save($order_id, 0, $this->t('Gladepay reported a payment of @amount', ['@amount' => uc_currency_format($amount, FALSE),]));
         break;

       default:
        uc_order_comment_save($order_id, 0, $this->t("The customer's attempted payment from Gladepay failed."), 'admin');
         return $this->redirect('uc_cart.cart');
        break;
     }
     $session = \Drupal::service('session');
     $session->set('uc_checkout_complete_' . $order->id(), TRUE);

    //redirect and change transaction status to 'Payment Received'
    return $this->redirect('uc_cart.checkout_complete');

  }

}