<?php

namespace Drupal\uc_gladepay\Plugin\Ubercart\PaymentMethod;

//ubercart Form Interfaces. This plugin implements offsite payment method
use Drupal\Core\Form\FormStateInterface; 
use Drupal\Core\Url;
use Drupal\uc_order\OrderInterface;
use Drupal\uc_payment\PaymentMethodPluginBase;
use Drupal\uc_payment\OffsitePaymentMethodPluginInterface;

/**
 * Defines the PayPal Payments Standard payment method.
 *
 * @UbercartPaymentMethod(
 *   id = "uc_gladepay",
 *   name = @Translation("GladePay Inline Checkout")
 * )
 */

class GladepayRedirect extends PaymentMethodPluginBase implements OffsitePaymentMethodPluginInterface {
  
  /**
   * {@inheritdoc}
   */

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    //set required configuration parameters when a developer installs the module (plugin) newly
    $pkey = $this->configuration['pkey'];
    $psalt = $this->configuration['psalt'];
    $pmode = $this->configuration['pmode'];

    //accepts merchant key
    $form['pkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Key'),
      '#default_value' => $pkey,
      '#description' => $this->t('Use "123456789" as Test Key'),
      '#required' => TRUE,
    ];


    //accepts merchant ID
    $form['psalt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $psalt,
      '#description' => $this->t('Use "GP0000001" as Test ID'),
      '#required' => TRUE,
    ];


    //lets have our script links put in a variable here
    $live_link = '<script type="text/javascript" src="https://api.gladepay.com/checkout.js"></script>';
    $test_link = '<script type="text/javascript" src="http://demo.api.gladepay.com/checkout.js"></script>';
    $form['pmode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#options' => array(
        "'.$test_link.'" => t('TEST'),
        "'.$live_link.'" => t('LIVE'),
      ),
      '#default_value' => $pmode,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['pkey'] = $values['pkey'];
      $this->configuration['psalt'] = $values['psalt'];
      $this->configuration['pmode'] = $values['pmode'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildRedirectForm(array $form, FormStateInterface $form_state, OrderInterface $order = NULL) {
    //SUBMIT THESE INFORMATION TO GLADEPAY INLINE CHECKOUT HERE AND LATER FIND REDIRECT PAGE NO NEED BUILDING ANY FORM
    $key = $this->configuration['pkey']; //merchant key
    $mid = $this->configuration['psalt']; //merchant id
    $txnmod = $this->configuration['pmode']; //gets either live link or demo link
    $txnid = $order->id(); //transaction ID
    $address = $order->getAddress('billing');
    $firstname = substr($address->first_name, 0, 128); //customer firstname
    $lastname = substr($address->last_name, 0, 128); //customer lastname
    $amount = uc_currency_format($order->getTotal(), FALSE, FALSE, '.'); //total amount
    $country = substr($address->country, 0, 128); //country
    $email = substr($order->getEmail(), 0, 64); //customer email
    
     $redirect_url = Url::FromRoute('uc_gladepay.complete', ['order_id' => $order->id(),], ['absolute' => TRUE])->toString();

     //set inline checkout js src for test and live accounts
    if($mid==="GP0000001"){
      $inline_js = "http://demo.api.gladepay.com/checkout.js";
    }else{
      $inline_js = "http://api.gladepay.com/checkout.js";
    }

    //CURRENCY CONVERSION MODULE IS GOOD SO GET IT

    //$txnid passed as 'firstname' because it is needed to clear cache after payment is complete in GladepayController


    //inline checkout modal
    $html = '
      <!DOCTYPE html>
      <html>
          <body onload="pay()">

              <script type="text/javascript" src="'.$inline_js.'"></script>
              <script>

                    function pay(){
                      initPayment({
                      MID:"'.$mid.'",
                      email: "'.$email.'",
                      firstname:"'.$firstname.'",
                      lastname: "'.$lastname.'",
                      description: "'.$txnid.'",
                      title: "",
                      amount: "'.$amount.'",
                      country: "'.$country.'",
                      currency: "NGN",
                      onclose: function() {

                      },
                      callback: function(response) {
                        var gladepay_response = JSON.stringify(response);    
                        window.location.href = "'.$redirect_url.'&response="+gladepay_response+"&order_id='.$txnid.'";
                      }
                  });
              }

              </script>
      </body>
      </html>
'; 

//trigger inline checkout modal
echo $html;

  }

}
